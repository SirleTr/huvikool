﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;

namespace Huvikool.Controllers
{
    public class GroupsController : Controller



    {
        private TagaridaEntities5 db = new TagaridaEntities5();

        //GET: Groups
        public ActionResult Index(int id = 0)
        {
            var groups = db.Groups.Include(g => g.InterestArea);
            return View(groups.ToList());


        }
 
 
        // GET: Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }

            var juhendajad = (new TagaridaEntities2()).AspNetRoles.Find("Juhendaja")?.AspNetUsers.Select(x => x.Email);

            ViewBag.Juhendaja =
                new SelectList(
                db.People.AsEnumerable()
                // pärast lisame siia
                .Where(y => juhendajad.Contains(y.Email))
                .ToList(), "Id", "FullName");

  
            var Osaleja = (new TagaridaEntities5()).Members.Where(z => z.GroupId == id).Select(p => p.PersonId);
            ViewBag.Osalejad = db.People.AsEnumerable().Where(p => Osaleja.Contains(p.Id)).ToList();


            return View(group);
        }




        [Authorize(Roles = "Admin")]
        public ActionResult LisaJuhendaja(int id, int Juhendaja) // 
        {

            Group g = db.Groups.Find(id);
            Person j = db.People.Find(Juhendaja);
            if (g != null && j != null)
            {
                try
                {
                    g.Instructors.Add(j);
                    db.SaveChanges();
                }
                catch { }
            }


            return RedirectToAction("Details", new { id });
        }

        [Authorize(Roles = "Admin")]
        public ActionResult EemaldaJuhendaja(int id, int Juhendaja) // 
        {

            Group g = db.Groups.Find(id);
            Person j = db.People.Find(Juhendaja);
            if (g != null && j != null)
            {
                try
                {
                    g.Instructors.Remove(j);
                    db.SaveChanges();
                }
                catch { }
            }


            return RedirectToAction("Details", new { id });
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            ViewBag.InterestAreaId = new SelectList(db.InterestAreas, "Id", "Name");
            return View();
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,InterestAreaId,Name,/*StartDate,EndDate,*/MaxMembers,PictureId")] Group group)
        {
            if (ModelState.IsValid)
            {
                db.Groups.Add(group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.InterestAreaId = new SelectList(db.InterestAreas, "Id", "Name", group.InterestAreaId);
            return View(group);
        }

        // GET: Groups/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            ViewBag.InterestAreaId = new SelectList(db.InterestAreas, "Id", "Name", group.InterestAreaId);

            var juhendajad = (new TagaridaEntities2()).AspNetRoles.Find("Juhendaja")?.AspNetUsers.Select(x => x.Email);

            ViewBag.Juhendaja =
                new SelectList(
                db.People.AsEnumerable()
                // pärast lisame siia
                .Where(y => juhendajad.Contains(y.Email))
                .ToList(), "Id", "FullName");


            return View(group);

        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "Id,InterestAreaId,Name,StartDate,EndDate,, Description,MaxMembers,PictureId")] Group group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(group).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.InterestAreaId = new SelectList(db.InterestAreas, "Id", "Name", group.InterestAreaId);
            return View(group);
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Group group = db.Groups.Find(id);
            db.Groups.Remove(group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
