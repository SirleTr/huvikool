﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;
using System.IO;

namespace Huvikool.Controllers
{

    public class MyController : Controller
    {
        protected TagaridaEntities5 db = new TagaridaEntities5();
        [OutputCache(Duration = 3600, VaryByParam = "id", Location = System.Web.UI.OutputCacheLocation.Any)]
        public ActionResult Content(int? id)
        {
            DataFile df = db.DataFiles.Find(id);
            if (df == null) return HttpNotFound();
            return File(df.Content, df.ContentType);
        }

        public Person CurrentPerson = null; // siis on hea kohe see PersonObject omale taskusse panna
        public void CheckPerson()
        {
            if (Request.IsAuthenticated)        // kontrollida vaja vaid siis
                if ((CurrentPerson?.Email ?? "") != User.Identity.Name)    // kui juba ei ole meeles või on vale
                    using (TagaridaEntities5 db = new TagaridaEntities5())
                    {
                        CurrentPerson = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                        if (CurrentPerson == null)
                        {
                            using (ApplicationDbContext dba = new ApplicationDbContext())
                            {
                                ApplicationUser u = dba.Users.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                                db.People.Add(CurrentPerson = new Person
                                {
                                    Email = User.Identity.Name,
                                    FirstName = u.FirstName,
                                    LastName = u.LastName,
                                    BirthDate = u.BirthDate,

                                });
                                db.SaveChanges();
                            }
                        }
                    }
                else CurrentPerson = null;
        }
        protected void ChangeDataFile(HttpPostedFileBase file, Action<int> change, int? oldId)
        {
            if (file != null && file.ContentLength > 0)
                using (BinaryReader br = new BinaryReader(file.InputStream))
                {
                    DataFile df = new DataFile
                    {
                        Content = br.ReadBytes(file.ContentLength),
                        FileName = file.FileName.Split('\\').Last().Split('/').Last(),
                        ContentType = file.ContentType,
                        Created = DateTime.Now
                    };
                    db.DataFiles.Add(df);
                    db.SaveChanges();
                    change(df.Id);
                    if (oldId.HasValue)
                    {
                        db.DataFiles.Remove(db.DataFiles.Find(oldId.Value));
                        db.SaveChanges();
                    }

                }
        }

    }




    public class HomeController : MyController
    {
        public ActionResult Index()
        {
            CheckPerson();
            if (Request.IsAuthenticated)
            {
                TagaridaEntities5 db = new TagaridaEntities5();
                //ViewBag.Kasutaja = User.Identity.Name;
                //Person p = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                //if (p == null)
                //    return RedirectToAction("Create", "People", new { email = User.Identity.Name });
                //    //ViewBag.Person = "Meile tundmatu";
                //else
                //    ViewBag.Person = p.FirstName;


            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Huvikool on loodud aastal 2017.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Võta meiega ühendust või astu läbi!";

            return View();
        }
    }
}