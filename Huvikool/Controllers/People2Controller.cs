﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;



namespace Huvikool.Controllers
{


    public class People2Controller : MyController
    {
        //private TagaridaEntities5 db = new TagaridaEntities5();

        // GET: People2
        [Authorize(Roles ="Lapsevanem,Admin,Juhendaja")]
        public ActionResult Index(string id = "")
        {

       
            var rolenames = id.Split(',');


            var persoonid = db.People;
             
                   


            var members = db.Members.ToList();
            var groups = db.Groups.ToList();

            if (User.IsInRole("Admin"))
            //return View(persoonid);
            {
                ViewBag.id = new SelectList((new TagaridaEntities2()).AspNetRoles.Select(x => x.Name), id);
                if (id == "") return View(persoonid.ToList());
                var userEmails = (new TagaridaEntities2())
                    .AspNetRoles
                    .ToList()
                    .Where(x => rolenames.Contains(x.Name))
                    .Select(x => x.AspNetUsers)
                    .SelectMany(x => x)
                    .Select(x => x.Email).ToList();
                return View(persoonid
                    .AsEnumerable()
                    .Where(x => userEmails.Contains(x.Email)).ToList());
            }
            if (User.IsInRole("Lapsevanem")) 
                return View(persoonid.Where(x => x.Parents.Select(y => y.Email).Contains(User.Identity.Name)).ToList());

            // kui on juhendaja, siis tuleks talle näidata
            // rühmade kaupa tema rühmade õpilasi
            //if (CurrentPerson.InstructedGroups.Count() > 0) 
            //    return View(groups.Where(x=> x.Instructors.Select(y=> y.Email).Contains(User.Identity.Name)).ToList());
 

            return RedirectToAction("Index", "Home");
        }

    


        [Authorize(Roles = "Lapsevanem, Admin, Juhendaja")]
        public ActionResult AddGr(int id, int grId)
        {
            Person p = db.People.Find(id);
            Group gr = db.Groups.Find(grId);
            if (p != null && gr != null)
            {
                try
                {
                    p.Members.Add(new Member { GroupId = grId , StartDate = DateTime.Today});
                    db.SaveChanges();
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction("Details", new { id = id });
        }

        [Authorize(Roles = "Lapsevanem, Admin, Juhendaja")]
        public ActionResult RemoveGr(int id, int grId)
        {
            Person p = db.People.Find(id);
            Group gr = db.Groups.Find(grId);
            if (p != null && gr != null)
            {
                try
                {
                    Member m = p.Members.Where(x => x.GroupId == grId && x.EndDate == null).SingleOrDefault();
                    if (m != null)
                    {
                        m.EndDate = DateTime.Today;
                        db.SaveChanges();
                    }
                }
                catch (Exception)
                {

                }
            }
            //            return View("Details", u);
            return RedirectToAction("Details", new { id = id });
        }



        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            var activities = (new TagaridaEntities5()).Members.Where(x => x.GroupId == id).Select(p => p.PersonId);
            ViewBag.Activities = db.Activities.AsEnumerable()
                
                .Where(p => activities.Contains(p.Id))
                .ToList()
                ;


            //ViewBag.Groups = db.Groups.ToList()
            //   .Except(person.Groups.ToList())
            //   .ToList();


            return View(person);
        }

        // GET: People2/Create
        [Authorize]
        public ActionResult Create()
        {
            return View(
                
                );
        }

        // POST: People2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId,IK")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                ChangeDataFile(
                   file,
                   x => { person.PictureId = x; db.SaveChanges(); },
                   person.PictureId);


                if (User.IsInRole("Lapsevanem"))
                {
                    Person vanem = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();
                    vanem.Children.Add(person);
                    db.SaveChanges();
 

                }


                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            var pGroups = person.Members.Select(x => x.GroupId).ToList();
                        
            ViewBag.Groups = db.Groups.Where(z => z.MaxMembers > z.Members.Count()).ToList()
            //   .Except(person.Groups.ToList())
                .Where(x => !pGroups.Contains(x.Id))
               .ToList()
            ;


            return View(person);
        }

        // POST: People2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId,IK")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                ChangeDataFile(
                              file,
                              x => { person.PictureId = x; db.SaveChanges(); },
                              person.PictureId);
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
