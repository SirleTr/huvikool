﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;

namespace Huvikool.Controllers
{
    public class InterestAreasController : MyController
    {
        private TagaridaEntities5 db = new TagaridaEntities5();

        // GET: InterestAreas
        public ActionResult Index()
        {
            var interestAreas = db.InterestAreas;//.Include(s => s.DataFile);
    
            return View(db.InterestAreas.ToList());
        }

        // GET: InterestAreas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InterestArea interestArea = db.InterestAreas.Find(id);
            if (interestArea == null)
            {
                return HttpNotFound();
            }
            return View(interestArea);
        }

        // GET: InterestAreas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InterestAreas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description, PictureId")] InterestArea interestArea, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.InterestAreas.Add(interestArea);
                db.SaveChanges();
                ChangeDataFile(
                file,
                x => { interestArea.PictureId = x; db.SaveChanges(); },
                interestArea.PictureId);
                return RedirectToAction("Index");
            }

            return View(interestArea);
        }

        // GET: InterestAreas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InterestArea interestArea = db.InterestAreas.Find(id);
            if (interestArea == null)
            {
                return HttpNotFound();
            }
            return View(interestArea);
        }

        // POST: InterestAreas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description, PictureId")] InterestArea interestArea, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(interestArea).State = EntityState.Modified;
                db.SaveChanges();
                ChangeDataFile(
               file,
               x => { interestArea.PictureId = x; db.SaveChanges(); },
               interestArea.PictureId);
                return RedirectToAction("Index");
            }
            return View(interestArea);
        }

        // GET: InterestAreas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InterestArea interestArea = db.InterestAreas.Find(id);
            if (interestArea == null)
            {
                return HttpNotFound();
            }
            return View(interestArea);
        }

        // POST: InterestAreas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InterestArea interestArea = db.InterestAreas.Find(id);
            db.InterestAreas.Remove(interestArea);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
