﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;

namespace Huvikool.Controllers
{
    public class ActivitiesController : Controller
    {
        private TagaridaEntities5 db = new TagaridaEntities5();

        // GET: Activities
        //public ActionResult Index()
        //{
        //    var activities = db.Activities.Include(a => a.Group).Include(a => a.Person).Include(a => a.Space);
        //    return View(activities.ToList());
        //}
        public ActionResult Index(int? month, int? year)
        {
            int monthv = month ?? DateTime.Today.Month;
            int yearv = year ?? DateTime.Today.Year;
            // kaks parameetrit, puudumisel võtame jooskva aasta ja/või jooksva kuu

            DateTime startdate = new DateTime(yearv, monthv, 1); // kuu algus
            DateTime calstart = startdate.AddDays(-(((int)startdate.DayOfWeek + 6) % 7));
            // mis kuupäevast peaks algama kalender (mul kulus hea 15 min selle valmi jaoks)

            // kõik 4 paneme viewsse kaasa
            ViewBag.StartDate = startdate;
            ViewBag.CalStart = calstart;
            ViewBag.Month = monthv;
            ViewBag.Year = yearv;

            // Nüüd teeme kuus nädalat kuupäevi
            ViewBag.Dates = Enumerable
                .Range(0, 42)               // kuus nädalat (42)
                .GroupBy(x => x / 7)        // grupeerime 7 kaupa
                .Select(x => x.Select(y => calstart.AddDays(y)).ToList())
                // iga grupi teisendame kuupäevade listiks
                .ToList()       // ja kõik need grupid paneme omakorda listi
                ;
            // tulemuseks on umbes Datetime[6][7] massiiv, aga tegelikult List<List<DateTime>>

            // aga viewle mudeliks anname oma taskid - läheme nüüd seda viewd vaatama
            return View(db.Activities
                //                .AsEnumerable()
                //                .Where(x => x.StartDate?.Year <= yearv && x.EndDate?.Year >= yearv)
                .ToList());
        }


        // GET: Activities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }

            //var Activities = db.Activities.Where(g => g.GroupId == id).ToList();

            //var activities = db.Activities.Include(a => a.Group).Include(a => a.Person).Include(a => a.Space)
            //    .Where(z => z.GroupId == id).Select(p => p.Person);

            //var Osaleja = db.Members.Where(z => z.GroupId == id).Select(p => p.PersonId);

            //ViewBag.Osalejad = db.Members
            //    .AsEnumerable()
            //    .Where(m => Activities.Contains().Select(p=> p.Members))
            //    .ToList()
            //    ;

            var Osaleja = (new TagaridaEntities5()).Members.Where(z => z.GroupId == activity.GroupId).Select(p => p.PersonId);
            ViewBag.Osalejad = db.People.AsEnumerable().Where(p => Osaleja.Contains(p.Id)).ToList();







            return View(activity);
        }

        // GET: Activities/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.Organizer = new SelectList(db.People, "Id", "Email");
            ViewBag.SpaceId = new SelectList(db.Spaces, "Id", "Name");
            return View();
        }

        // POST: Activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GroupId,SpaceId,StartDate,EndDate,Name,Organizer,Description")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Activities.Add(activity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", activity.GroupId);
            ViewBag.Organizer = new SelectList(db.People, "Id", "Email", activity.Organizer);
            ViewBag.SpaceId = new SelectList(db.Spaces, "Id", "Name", activity.SpaceId);
            return View(activity);
        }

        // GET: Activities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", activity.GroupId);
            ViewBag.Organizer = new SelectList(db.People, "Id", "Email", activity.Organizer);
            ViewBag.SpaceId = new SelectList(db.Spaces, "Id", "Name", activity.SpaceId);
            return View(activity);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GroupId,SpaceId,StartDate,EndDate,Name,Organizer,Description")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", activity.GroupId);
            ViewBag.Organizer = new SelectList(db.People, "Id", "Email", activity.Organizer);
            ViewBag.SpaceId = new SelectList(db.Spaces, "Id", "Name", activity.SpaceId);
            return View(activity);
        }

        // GET: Activities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Activity activity = db.Activities.Find(id);
            db.Activities.Remove(activity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
