﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Huvikool.Models;

namespace Huvikool.Controllers
{
    public class MembersController : Controller
    {
        private TagaridaEntities5 db = new TagaridaEntities5();


        void Prep(int Group)
        {
            var members = db.Members.Select(x => new { x.Group});
            var groups = members.Select(x => x.Group).Distinct().ToList();
            ViewBag.Group = new SelectList(groups, Group);


        }


    // GET: Members
    public ActionResult Index(int? id)
        {

            ViewBag.id = new SelectList(db.Groups, "Id", "Name", id);

            var members= db.Members.Include(p => p.Group)
                ;
            ViewBag.Ret = id;
            ViewBag.Name = db.Groups.Find(id)?.Name ?? "";

            if(id == null) return View(members.OrderBy(l => l.Person.LastName).ToList());

            //Prep(GruppId);
            Group group = db.Groups.Find(id);

            return View(group?.Members
                .ToList())
                ;


            //return View(members.OrderBy(l => l.Person.LastName).ToList());
    //        return View(db.Customers
    //.Where(x => (Country == "" || x.Country == Country) && (City == "" || x.City == City))
    //.ToList());


        }

        // GET: Members/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        //// GET: Members/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email");
            return View();
        }

        // POST: Members/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,StartDate,EndDate,GroupId")] Member member)
        {
            if (ModelState.IsValid)
            {
                db.Members.Add(member);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", member.GroupId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", member.PersonId);
            return View(member);
        }

        // GET: Members/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", member.GroupId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", member.PersonId);
            return View(member);
        }

        // POST: Members/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,StartDate,EndDate,GroupId")] Member member)
        {
            if (ModelState.IsValid)
            {
                db.Entry(member).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", member.GroupId);
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", member.PersonId);
            return View(member);
        }

        // GET: Members/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return View(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Member member = db.Members.Find(id);
            db.Members.Remove(member);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
