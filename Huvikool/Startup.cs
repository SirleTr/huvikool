﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Huvikool.Startup))]
namespace Huvikool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
