﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Huvikool.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [Display(Name = "Jäta mind meelde?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} tähemärki pikk", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Sisestatud salasõnad ei klapi")]
        public string ConfirmPassword { get; set; }


        [Required]
        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Perekonnanimi")]
        public string LastName { get; set; }

        public string Displayname { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public System.DateTime BirthDate { get; set; } = new System.DateTime(2000, 1, 1);
        public string FullName => $"{FirstName} {LastName}";



    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} peab olema vähemalt {2} tähemärki pikk", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Salasõna")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Kinnita salasõna")]
        [Compare("Password", ErrorMessage = "Sisestatud salasõnad ei klapi")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
