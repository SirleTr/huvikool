//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Huvikool.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class InterestArea
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InterestArea()
        {
            this.Groups = new HashSet<Group>();
            this.Teachers = new HashSet<Person>();
        }
    
        public int Id { get; set; }
        [Display(Name = "Huviring")]
        public string Name { get; set; }
        [Display(Name = "Huviringi kirjeldus")]
        public string Description { get; set; }
        [Display(Name = "Foto")]
        public Nullable<int> PictureId { get; set; }
    
        public virtual DataFile DataFile { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Group> Groups { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Person> Teachers { get; set; }
    }
}
